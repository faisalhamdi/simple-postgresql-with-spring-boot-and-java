**Follow the instruction below :**

1. Create Project with Spring Boot
* Head over to http://start.spring.io.
* Enter postgres-demo in the Artifact field.
* Add Web, JPA and PostgreSQL in the dependencies section.
* Click Generate to download the project.
* Put the project to folder as you like

2. Configuration Spring Boot with PostgreSQL
* open file src/main/resources/application.properties
* add postgreSQL database url, username and password like as below

    * spring.datasource.url=jdbc:postgresql://{host}:{port}/{database_name}
    * spring.datasource.username={username}
    * spring.datasource.password={password}

